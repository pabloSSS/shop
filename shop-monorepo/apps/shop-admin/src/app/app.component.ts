import { Component } from '@angular/core';

@Component({
  selector: 'shop-monorepo-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'shop-admin';
}
